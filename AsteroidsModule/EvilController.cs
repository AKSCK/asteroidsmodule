﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AsteroidsModule
{
    public class EvilController : MonoBehaviour
    {
        public string TagName = "Evil";
        public int EvilCount = 5;
        public List<Evil> Evils;

        // Update is called once per frame
        protected void Update()
        {
            if (GameObject.FindGameObjectsWithTag(TagName).Length >= EvilCount)
            {
                return;
            }
            foreach (var ev in Evils)
            {
                var rand = Random.Range(1, EvilCount);
                for (int i = 1; i <= rand; i++)
                {
                    var evGO = GameObject.Instantiate(ev, GameController.GetRandomVectorForCamera(this.transform.position.z), new Quaternion());
                }
            }
        }



    }
}
