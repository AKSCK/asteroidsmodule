﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AsteroidsModule
{
    public class PlayObject : MonoBehaviour
    {
        public bool IsDestroy = false;
        /// <summary>
        /// Ширина объекта, служит для того, чтобы объекты появлялись и перемещались не сразу 
        /// </summary>
        public float ObjectWidth = 0;

        // Update is called once per frame
        protected void Update()
        {
            if (IsDestroy)
            {
                DestroyCheck();
            }
            else
            {
                ReturnCheck();
            }
        }
        /// <summary>
        /// Уничтожаю сам себя в случае вылета за карту (на данный момент ипользуется для пуль)
        /// </summary>
        void DestroyCheck()
        {
            var pos = this.transform.position;
            var camera = Camera.main;
            float width = camera.pixelWidth;
            float height = camera.pixelHeight;
            Vector2 camPosition = camera.ScreenToWorldPoint(new Vector2(width, height));
            if (pos.x > camPosition.x
                || pos.x < camPosition.x * -1
                || pos.y > camPosition.y
                || pos.y < camPosition.y * -1)
            {
                GameObject.Destroy(this.gameObject);
            }
        }
        /// <summary>
        /// Возвращаю себя в камеру с другой стороны
        /// </summary>
        void ReturnCheck()
        {
            var pos = this.transform.position;
            var camera = Camera.main;
            float width = camera.pixelWidth;
            float height = camera.pixelHeight;
            Vector2 camPosition = camera.ScreenToWorldPoint(new Vector2(width, height));
            if (pos.x > camPosition.x + ObjectWidth)
            {
                pos.x = camPosition.x * -1 - ObjectWidth;
            }
            else if (pos.x < camPosition.x * -1 - ObjectWidth)
            {
                pos.x = camPosition.x + ObjectWidth;
            }
            if (pos.y > camPosition.y + ObjectWidth)
            {
                pos.y = camPosition.y * -1 - ObjectWidth;
            }
            else if (pos.y < camPosition.y * -1 - ObjectWidth)
            {
                pos.y = camPosition.y + ObjectWidth;
            }
            this.transform.position = pos;
        }
    }
}