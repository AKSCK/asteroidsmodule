﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AsteroidsModule
{
    public class LazerGun : MonoBehaviour
    {
        public GameObject LazerObject;
        public int LazerMaxCount = 0;

        int lazerCount = 0;
        int LazerCount
        {
            get
            {
                return lazerCount;
            }
            set
            {
                GameController.Controller.UpdateLazerCount(value);
                lazerCount = value;
            }
        }
        bool isLazerUsed = false;
        // Start is called before the first frame update
        protected void Start()
        {
            LazerCount = LazerMaxCount;
            LazerObject.SetActive(false);
            InvokeRepeating("ReloadCountLazer", 0, 5f);
        }

        // Update is called once per frame
        protected void Update()
        {
            if (Input.GetButton("Fire1") && !isLazerUsed && LazerCount > 0)
            {
                LazerCount--;
                ShotLazer();
            }

        }
        /// <summary>
        ///Возобнавление количества выстрелов лазером
        /// </summary>
        void ReloadCountLazer()
        {
            if (LazerCount < LazerMaxCount)
                LazerCount++;
        }
        /// <summary>
        ///Выстрел лазером
        /// </summary>
        void ShotLazer()
        {
            isLazerUsed = true;
            LazerObject.SetActive(true);
            Invoke("StopLazer", 0.1f);
        }
        /// <summary>
        /// Остановка выстрела
        /// </summary>
        void StopLazer()
        {
            LazerObject.SetActive(false);
            Invoke("ReloadLazer", 2);
        }
        /// <summary>
        /// Перезарядка лазера
        /// </summary>
        void ReloadLazer()
        {
            isLazerUsed = false;
        }

    }
}