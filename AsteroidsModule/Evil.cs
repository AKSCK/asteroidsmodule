﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AsteroidsModule
{
    public class Evil : PlayObject
    {
        public int Speed = 10;
        public int ScoreAdd = 1;
        // Start is called before the first frame update

        protected void Start()
        {
            GameController.AddUniversalForce(this.gameObject, GameController.GetRandomVectorInCamera() * Speed);
        }

        protected void OnTriggerEnter2D(Collider2D collider2D)
        {
            OnTrigger(collider2D.gameObject);
        }
        /// <summary>
        /// Проверяю столкновение, если пуля то уничтожаюсь и уничтожаю пулю, если лазер то уничтожаю только себя
        /// </summary>
        /// <param name="collider"></param>
        void OnTrigger(GameObject collider)
        {
            if (collider.tag == "Bullet")
            {
                GameController.Controller.AddScore(ScoreAdd);
                Destroy(collider.gameObject);
                Destroy(this.gameObject);
            }
            if (collider.name == "Lazer")
            {
                GameController.Controller.AddScore(ScoreAdd);
                Destroy(this.gameObject);
            }
        }
        protected void OnTriggerEnter(Collider collider2D)
        {
            OnTrigger(collider2D.gameObject);
        }

    }
}
