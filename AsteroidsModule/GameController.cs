﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
namespace AsteroidsModule
{
    public abstract class GameController : MonoBehaviour
    {
        public static GameController Controller;
        public GameObject DeadPanel;
        protected Player Player;
        public static int Score;
        public static bool IsStart = false;

        public abstract void UpdateLazerCount(int lazerCount);
        public abstract void AddScore(int scoreAdd);


        // Start is called before the first frame update
        protected void Start()
        {
            Controller = this;
            if (!IsStart)
            {
                IsStart = true;
                DeadPanel.SetActive(true);
            }
            else
            {
                 Instantiate(Player, this.transform.position, this.transform.rotation);

            }
        }

        // Update is called once per frame
        protected void Update()
        {
            if (Input.GetButton("Submit"))
            {
                if (DeadPanel.activeSelf)
                {
                    NewGame();
                }
            }
        }
        /// <summary>
        /// Переопределяется добавляется изменение 2D чекбокса 
        /// </summary>
        public virtual void PlayerDead()
        {
            DeadPanel.SetActive(true);
        }
        
        public void NewGame()
        {
            Score = 0;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        /// <summary>
        /// Метод для абстрагирования от 3д и 2д режима
        /// </summary>
        /// <param name="gameObject">Обект к которуму нужно приложить силу</param>
        /// <param name="Vector3">Вектор направления силы</param>
        public static void AddUniversalForce(GameObject gameObject, Vector3 Vector3) {
            var rg2D = gameObject.GetComponent<Rigidbody2D>();
            if (rg2D != null)
                rg2D.AddForce(Vector3, ForceMode2D.Force);
            else
            {
                var rg3D = gameObject.GetComponent<Rigidbody>();
                rg3D.AddForce(Vector3, ForceMode.Force);
            }
        }
        /// <summary>
        /// Метод для абстрагирования от 3д и 2д режима
        /// </summary>
        /// <param name="gameObject">Обект к которуму нужно приложить силу</param>
        /// <param name="Vector3">Вектор направления силы</param>
        public static void AddUniversalImpulse(GameObject gameObject, Vector3 Vector3)
        {
            var rg2D = gameObject.GetComponent<Rigidbody2D>();
            if (rg2D != null)
                rg2D.AddForce(Vector3, ForceMode2D.Impulse);
            else
            {
                var rg3D = gameObject.GetComponent<Rigidbody>();
                rg3D.AddForce(Vector3, ForceMode.Impulse);
            }
        }
        /// <summary>
        /// Генератор рандомных координат внутри камеры
        /// </summary>
        /// <returns>Рандомный вектор внутри камеры</returns>
        static public Vector3 GetRandomVectorInCamera()
        {
            var camera = Camera.main;
            float width = camera.pixelWidth;
            float height = camera.pixelHeight;
            Vector2 camPosition = camera.ScreenToWorldPoint(new Vector2(width, height));
            var randomX = Random.Range(camPosition.x * -1, camPosition.x);
            var randomY = Random.Range(camPosition.y * -1, camPosition.y);
            return new Vector3(randomX, randomY);
        }
        /// <summary>
        /// Генератор рандомных координат за камерой
        /// </summary>
        /// <returns>Рандомный вектор за камерой</returns>
        static public Vector3 GetRandomVectorForCamera(float z)
        {
            var camera = Camera.main;
            float width = camera.pixelWidth;
            float height = camera.pixelHeight;
            Vector2 camPosition = camera.ScreenToWorldPoint(new Vector2(width, height));
            var randomX = Random.Range(camPosition.x * -1, camPosition.x);
            var randomY = Random.Range(camPosition.y * -1, camPosition.y);
            if (randomX > 0)
            {
                randomX += camPosition.x;
            }
            else
            {
                randomX -= camPosition.x;
            }
            if (randomY > 0)
            {
                randomY += camPosition.y;
            }
            else
            {
                randomY -= camPosition.y;
            }
            return new Vector3(randomX, randomY, z);

        }

    }
}
