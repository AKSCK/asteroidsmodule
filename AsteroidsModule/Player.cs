﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AsteroidsModule
{
    public class Player : PlayObject
    {
        public static Player Now;
        //Параметры игрока
        public float RotationSpeed = -4;
        public float Speed = 10;
        public float BulletSeed = 50;

        //Доступное оружие
        public PlayObject Bullet;



        // Start is called before the first frame update
        protected void Start()
        {
            Now = this;
            Bullet.gameObject.SetActive(false);

        }
        // Update is called once per frame
        protected new void Update()
        {
            base.Update();

            if (Input.GetButtonUp("Jump"))
            {
                Shot();
            }


            float verticalAxis = Input.GetAxis("Vertical") * Speed;
            if (verticalAxis > 0)
            {
                GameController.AddUniversalForce(this.gameObject, this.transform.up * verticalAxis);
            }

            float horizontalAxis = Input.GetAxis("Horizontal") * RotationSpeed;
            transform.Rotate(0, 0, horizontalAxis);

        }
        protected void OnTriggerEnter2D(Collider2D collider2D)
        {
            EvelCheck(collider2D.gameObject);
        }
        protected void OnTriggerEnter(Collider collider)
        {
            EvelCheck(collider.gameObject);
        }
        /// <summary>
        /// Проверяю столкновение с астероидом или летающей тарелкой
        /// </summary>
        /// <param name="collider"></param>
        void EvelCheck(GameObject collider)
        {
            if (collider.tag == "Evil" || collider.tag == "UFO")
            {
                GameController.Controller.PlayerDead();
                Destroy(this.gameObject);
            }
        }
        /// <summary>
        /// Выстрел пулей
        /// </summary>
        void Shot()
        {
            var bull = GameObject.Instantiate(Bullet, this.transform.up + this.transform.position, this.transform.rotation);
            bull.gameObject.SetActive(true);
            GameController.AddUniversalForce(bull.gameObject, this.transform.up * BulletSeed);

        }


    }
}