﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AsteroidsModule
{
    public class UFO : Evil
    {


        // Start is called before the first frame update
        new protected void Start()
        {
            base.Start();
        }

        // Update is called once per frame
        new protected void Update()
        {
            base.Update();
            //Увожу летающую тарелку вверх после уничтожения игрока
            if (Player.Now == null)
            {
                GameController.AddUniversalImpulse(this.gameObject, new Vector2(0.05f, 0.05f));
                return;
            }

            Vector2 vector = Player.Now.transform.position - transform.position;
            if (Mathf.Abs(vector.y) < 10)
                GameController.AddUniversalImpulse(this.gameObject, vector / 100);
        }
    }
}  